'use strict';

/**
 * @ngdoc overview
 * @name protractorDemoApp
 * @description
 * # protractorDemoApp
 *
 * Main module of the application.
 */
angular
  .module('protractorDemoApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {

    $locationProvider.hashPrefix('')

    $routeProvider
      .when('/example1', {
        templateUrl: 'views/example1.html',
        controller: 'Example1Ctrl',
        controllerAs: 'ctrl'
      })
      .when('/example2', {
        templateUrl: 'views/example2.html',
        controller: 'Example2Ctrl',
        controllerAs: 'ctrl'
      })
      .when('/example3', {
        templateUrl: 'views/example3.html',
        controller: 'Example3Ctrl',
        controllerAs: 'ctrl'
      })
      .otherwise({
        redirectTo: '/example1'
      });
  }]);
