'use strict';

angular.module('protractorDemoApp')

// Example1
.controller('Example1Ctrl', ['$scope', '$timeout', function($scope, $timeout){

  $scope.form = {
    email: null,
    password: null
  }

  $scope.submit = function() {
    delete $scope.error
    delete $scope.success
    $scope.f.$setPristine()

    $timeout(function() {
      if ($scope.f.email.$valid) {
        if ($scope.f.password.$valid) {
          $scope.success = true
        } else {
          $scope.error = 'Invalid password'
        }
      } else {
        $scope.error = 'Invalid email'
      }
    }, 500)
  }

}])

// Example2
.controller('Example2Ctrl', ['$scope', function($scope){

  $scope.comments = []

  function _resetForm() {
    $scope.form = {
      name: null,
      comment: null
    }
  }

  $scope.submit = function() {
    $scope.comments.push(angular.copy($scope.form))
    _resetForm()
  }

  _resetForm()

}])

// Example3
.controller('Example3Ctrl', ['$scope', '$timeout', function($scope, $timeout){

  function _resetForm() {
    $scope.form = {
      name: null,
      email: null,
      password: null,
      confirm: null,
      role: 'guest',
      cool: false
    }
  }

  $scope.roles = [{
    label: 'Guest',
    value: 'guest'
  }, {
    label: 'User',
    value: 'user'
  }, {
    label: 'Admin',
    value: 'admin'
  }]

  $scope.submit = function() {
    $scope.f.$setPristine()
    delete $scope.error
    delete $scope.success

    $timeout(function() {
      if ($scope.f.name.$valid) {
        if ($scope.f.email.$valid) {
          if ($scope.f.password.$valid) {
            if ($scope.f.confirm.$valid && $scope.form.password === $scope.form.confirm) {
              if ($scope.f.role.$valid) {
                $scope.success = true
              } else {
                $scope.error = 'Invalid user role'
              }
            } else {
              $scope.error = 'Passwords do not match'
            }
          } else {
            $scope.error = 'Invalid password'
          }
        } else {
          $scope.error = 'Invalid email'
        }
      } else {
        $scope.error = 'Missing name'
      }
    }, 500)
  }

  _resetForm()

}])
