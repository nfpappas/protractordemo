'use strict';

angular.module('protractorDemoApp')
.controller('NavbarCtrl', ['$location', '$scope', function($location, $scope){

  $scope.isActive = function(path) {
    return path === $location.path()
  }

}])
