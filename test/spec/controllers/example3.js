'use strict';

describe('Controller: Example3Ctrl', function () {

  // load the controller's module
  beforeEach(module('protractorDemoApp'));

  var Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Ctrl = $controller('Example3Ctrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should list roles', function () {
    expect(scope.roles.length).toBe(3);
  });
});
